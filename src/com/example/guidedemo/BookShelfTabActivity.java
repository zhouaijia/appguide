package com.example.guidedemo;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import com.tadu.android.a.al;
import com.tadu.android.common.a.d;
import com.tadu.android.common.application.ApplicationData;
import com.tadu.android.common.util.dp;
import com.tadu.android.common.util.n;
import com.tadu.android.common.util.p;
import com.tadu.android.view.TDGroupActivity;
import com.tadu.android.view.customControls.TabButton;
import com.tadu.android.view.customControls.TabScrollView;
import com.tadu.android.view.customControls.TabWidget;
import com.tadu.android.view.customControls.e;
import java.util.ArrayList;

public class BookShelfTabActivity extends Activity{
  public static BookShelfTabActivity a = null;
  private static long b = 0L;
  private Button c;
  private int d = 0;
  private PopupWindow e;
  private boolean f = true;
  private int[][] g = { { 2130837722, 2130837723 }, { 2130837720, 2130837721 } };
  private View h = null;
  private View i = null;
  private ViewPager j;
  private e k;
  private ArrayList l = new ArrayList();
  private int[] m = { 2130837759, 2130837755 };
  private int[] n = { 2130837759, 2130837755 };
  private TabScrollView mTabScrollView;
  private TabWidget mTabWidget;

  private void a(int paramInt) {
    mTabScrollView.setTargetRow(paramInt);
    mTabWidget.setCurrentTab(paramInt);
    if (1 == c())
      b();
    if (paramInt == 0)
      this.d = 0;
    while (paramInt != 1)
      return;
    this.d = 1;
  }

  private void b(int paramInt){
    if (this.d == 0)
      am.b().d(paramInt);
    while (this.d != 1)
      return;
    a.b().d(paramInt);
  }

  public final void a(){
    if (this.d == 0)
      am.b().c();
    while (this.d != 1)
      return;
    a.b().c();
  }

  public final void a(boolean paramBoolean){
    if (dp.b("isRegister", false))
      new d().b(this, new bm(this, paramBoolean));
  }

  public final void b(){
    if (c() != 0)
    {
      b(0);
      this.c.setText(2131296321);
      PopupWindow localPopupWindow = this.e;
      int i1 = 0;
      if (localPopupWindow != null)
      {
        boolean bool = this.e.isShowing();
        i1 = 0;
        if (bool)
          i1 = 1;
      }
      if (i1 != 0)
      {
        this.e.dismiss();
        this.e = null;
      }
    }
  }

  public final int c(){
    if (this.d == 0)
      return am.b().e();
    if (this.d == 1)
      return a.b().e();
    return 0;
  }

  public final void d(){
    if (this.d == 0)
      am.b().d();
    while (this.d != 1)
      return;
    a.b().d();
  }

  public void onConfigurationChanged(Configuration paramConfiguration){
    super.onConfigurationChanged(paramConfiguration);
    onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle){
    super.onCreate(paramBundle);
    a = this;
    requestWindowFeature(1);
    setContentView(2130903072);
    n.d(this);
    this.j = ((ViewPager)findViewById(2131100017));
    this.h = new am(this).a();
    this.i = new a(this).a();
    this.l.add(this.h);
    this.l.add(this.i);
    this.k = new e(this.l, (byte)0);
    this.j.setOffscreenPageLimit(this.l.size());
    this.j.setAdapter(this.k);
    this.j.clearAnimation();
    this.j.setOnPageChangeListener(new bg(this));
    mTabScrollView = ((TabScrollView)findViewById(2131100014));
    mTabScrollView.setData(this.m.length, 111, 2130837731);//bookshelf_tab_select_bg
    mTabWidget = ((TabWidget)findViewById(2131100015));
    mTabWidget.setTabSelectionListener(new bh(this));
    for (int i1 = 0; i1 < this.m.length; i1++)
    {
      TabButton localTabButton = new TabButton(this);
      localTabButton.setTabIcon(this.m[i1], this.n[i1]);
      mTabWidget.addView(localTabButton);
    }
    this.c = ((Button)findViewById(2131100016));
    this.c.setOnClickListener(new bi(this));
    a(this.f);
    a(0);
  }

  protected void onDestroy() {
    super.onDestroy();
    ApplicationData.a().a();
    b = System.currentTimeMillis();
    if (1 == c())
      b();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 4)
    {
      if (dp.b("isTaduSettingGuideKey", dp.g.booleanValue()))
        p.a();
      if (1 == c())
        b();
    }
    while (paramInt == 84)
    {
      return true;
      if (TDGroupActivity.a().b())
      {
        com.tadu.android.common.e.a.a.a("[EXIT]", false);
        p.j(this);
        return true;
      }
      com.tadu.android.common.e.a.a.a("[T1]", false);
      TDGroupActivity.a().c();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent){
    super.onNewIntent(paramIntent);
    long l1 = System.currentTimeMillis() - b;
    this.f = false;
    if (l1 > 3000L)
      a(this.f);
  }

  protected void onPause(){
    super.onPause();
    n.d(this);
    ApplicationData.a().a();
    b = System.currentTimeMillis();
    if (1 == c())
      b();
  }

  protected void onRestart(){
    super.onRestart();
    n.d(this);
  }

  protected void onResume(){
    super.onResume();
    n.d(this);
    if (!dp.b("isTaduSettingGuideKey", dp.g.booleanValue()))
    {
      p.f(this);
      dp.a("isTaduSettingGuideKey", true);
    }
  }

  protected void onStart(){
    super.onStart();
    n.d(this);
  }

  protected void onStop(){
    super.onStop();
    n.d(this);
  }
}