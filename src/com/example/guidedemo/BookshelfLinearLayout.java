package com.example.guidedemo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.guidedemo.util.Utils;

public class BookshelfLinearLayout extends LinearLayout{
  private AbsListView mAbsListView;
  private View mView;
  private int c;
  private float d;
  private boolean e;
  private int f = 0;

  public BookshelfLinearLayout(Context context){
    super(context);
  }

  public BookshelfLinearLayout(Context context, AttributeSet attributeSet){
    super(context, attributeSet);
    this.c = Utils.getDensity(10.0F,context);
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent event){
    if (this.e){
      MotionEvent motionEvent = MotionEvent.obtain(event);
      motionEvent.setAction(0);
      this.e = false;
      return super.dispatchTouchEvent(motionEvent);
    }
    return super.dispatchTouchEvent(event);
  }

  @Override
  public boolean onInterceptTouchEvent(MotionEvent event){
    switch (event.getAction()){
    case 1:
    default:
    case 0:
    case 2:
    }
    while (true)
    {
      return super.onInterceptTouchEvent(event);
      this.d = event.getY();
      continue;
      float f1 = event.getY();
      if ((int)Math.abs(f1 - this.d) > this.c);
      int j;
      for (int i = 1; i != 0; i = 0)
      {
        j = getScrollY();
        if ((mAbsListView == null) || (mAbsListView.getChildCount() <= 0) || (mAbsListView.getFirstVisiblePosition() != 0) || (mAbsListView.getChildAt(0).getTop() != 0))
          break label207;
        if (event.getY() - this.d >= 0.0F)
          break label181;
        if (j >= mView.getHeight())
          break label162;
        this.f = 1;
        return true;
      }
      continue;
      label162: MotionEvent localMotionEvent = MotionEvent.obtain(event);
      localMotionEvent.setAction(0);
      return super.onInterceptTouchEvent(localMotionEvent);
      label181: if ((event.getY() - this.d > 0.0F) && (j >= 0))
      {
        this.f = 1;
        return true;
      }
      label207: this.d = f1;
    }
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {
    super.onLayout(changed, l, t, r, b);
    mView = getChildAt(0);
    if ((Utils.getBookShelfLayoutTop("bookshelfLayoutTop", false)) && (mAbsListView == null))
    	scrollTo(0, mView.getHeight());
    
    mAbsListView = (AbsListView)((FrameLayout)((FrameLayout)getChildAt(1)).getChildAt(1)).getChildAt(0);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    super.onMeasure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(
    		heightMeasureSpec + getChildAt(0).getMeasuredHeight()), View.MeasureSpec.getMode(heightMeasureSpec)));
  }

  @Override
  public boolean onTouchEvent(MotionEvent event){
   Utils.setBookShelfLayoutTop("bookshelfLayoutTop", true);
    
    
    int m;
    if (this.f != 1)
    {
      if ((int)Math.abs(event.getY() - this.d) <= this.c)
        break label154;
      m = 1;
      if (m == 0)
        break label179;
      int n = getScrollY();
      if ((mAbsListView != null) && (mAbsListView.getChildCount() > 0) && (mAbsListView.getFirstVisiblePosition() == 0) && (mAbsListView.getChildAt(0).getTop() == 0) && (event.getY() - this.d < 0.0F))
      {
        if (n >= mView.getHeight())
          break label160;
        this.f = 1;
      }
    }
    switch (event.getAction())
    {
    case 0:
    default:
    case 2:
    case 1:
    }
    while (true)
    {
      return true;
      label154: m = 0;
      break;
      label160: MotionEvent localMotionEvent = MotionEvent.obtain(event);
      localMotionEvent.setAction(0);
      return super.onTouchEvent(localMotionEvent);
      label179: return true;
      int i = getScrollY();
      int j = (int)(this.d - event.getY());
      int k = mView.getHeight();
      if (j < 0)
        if (i > 0)
        {
          if (-j > i)
            j = -i;
          scrollBy(0, j);
        }
      while (true)
      {
        this.d = event.getY();
        return true;
        if ((j > 0) && (i < k))
        {
          if (j > k - i)
            j = k - i;
          if (j == k - i)
            this.e = true;
          scrollBy(0, j);
        }
      }
      if (this.f == 1)
        this.f = 0;
    }
  }
}