package com.example.guidedemo;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;

public class DisplayConfig {
	private static DisplayMetrics mDisplayMetrics = null;
	private static Configuration config = null;
	
	public static int getWidthPixels() {
		return mDisplayMetrics.widthPixels;
	}
	
	public static int getSuitableDensity(int paramInt){
		float f1 = paramInt * mDisplayMetrics.density;
		float f2 = 0.5F;
		if (paramInt > 0){
			f2 = -0.5F;
		}
		return (int)(f2 + f1);
	}
	
	public static void initDisplayConfig(Context context) {
		mDisplayMetrics = context.getResources().getDisplayMetrics();
		config = context.getResources().getConfiguration();
	}
	
	public static void initDisplayConfig(Context context, Configuration configuration)  {
		mDisplayMetrics = context.getResources().getDisplayMetrics();
		config = configuration;
	}
	
	public static int getHeightPixels(){
		return mDisplayMetrics.heightPixels;
	}
	
	public static float getDensity(){
		return mDisplayMetrics.density;
	}
	
	public static int getDensityDpi() {
		return mDisplayMetrics.densityDpi;
	}
	
	public static String getDpi(){
		switch (getScreenWith()){
		default:
			return "";
		case 120:
			return "_ldpi";
		case 160:
			return "_mdpi";
		case 240:
			return "_hdpi";
		case 320:
			return "_xhdpi";
		case 480:
		}
		return "_xxhdpi";
	}
	
	public static int getScreenWith(){
		int i = mDisplayMetrics.densityDpi;
		if (i <= 120)
			return 120;
		if (i <= 160)
			return 160;
		if (i <= 240)
			return 240;
		if (i <= 320)
			return 320;
		if (i <= 400)
			return 320;
		return 480;
	}
}
