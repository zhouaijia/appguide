package com.example.guidedemo;

import android.app.Activity;
import android.os.Bundle;

public class DragableActivity extends Activity {
	@Override
	protected void onCreate(Bundle paramBundle){
	    super.onCreate(paramBundle);
	    setContentView(R.layout.dragable_layout);
	  }
}
