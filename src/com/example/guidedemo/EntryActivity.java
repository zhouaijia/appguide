package com.example.guidedemo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class EntryActivity extends Activity {
	private static final String ASSET_PATH_CHANNEL_LOGO = "channel_logo.png";
	
	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.activity_entry);
		((ImageView)findViewById(R.id.imageview_channellogo)).setImageBitmap(loadChannelLogo());
		new Handler().postDelayed(new Runnable() {
			public void run(){
				EntryActivity.this.startMainActivity();
			}
		}
		, 2000L);
	}
	
	private Bitmap loadChannelLogo(){
		try {
			Bitmap localBitmap = BitmapFactory.decodeStream(getResources().getAssets().open(ASSET_PATH_CHANNEL_LOGO));
			return localBitmap;
		}
		catch (Exception localException){
			localException.printStackTrace();
		}
		return null;
	}
	
	private void startMainActivity(){
		if (isNewVersion()) {
			startActivity(new Intent(this, GuideActivity.class));
		}else{
			startActivity(new Intent(this, MainActivity.class).setData(getIntent().getData()).putExtras(getIntent()));
		}
		finish();
	}
	
	private boolean isNewVersion(){
		return true;
	}
}
