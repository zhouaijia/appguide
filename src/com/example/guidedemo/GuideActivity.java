package com.example.guidedemo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GuideActivity extends Activity implements
		ViewPager.OnPageChangeListener, View.OnClickListener {

	private static final String TAG = GuideActivity.class.getName();
	private static final int DURATION_ANIM = 300;
	private static final int GUIDE_DOT_IMAGE_WIDTH = 14;
	private MyPagerAdapter mAdapter;
	private int mDotPos = 0;
	private LinearLayout mGuideDotsView;
	private TextView mGuideEntry;
	private List<View> mGuideImages = new ArrayList<View>();
	private ImageView mImageDot;
	private boolean mIsShowGuidePage = false;
	private int mOffset;
	private ViewPager mViewPager;
	
	@Override
	protected void onCreate(Bundle paramBundle){
	    super.onCreate(paramBundle);
	    setContentView(R.layout.guide_page_layout);
	    DisplayConfig.initDisplayConfig(this);
	    this.mOffset =  DisplayConfig.getSuitableDensity(GUIDE_DOT_IMAGE_WIDTH);
	    this.mImageDot = ((ImageView)findViewById(R.id.imageview_dot));
	    this.mGuideDotsView = ((LinearLayout)findViewById(R.id.layout_dots));
	    initGuideImageView();
	    initImageDot();
	    setPageAdapter();
	  }
	
	private void initGuideImageView(){
		this.mGuideImages.add(getGuideImageView(R.drawable.img_guide_background_1));
		this.mGuideImages.add(getGuideImageView(R.drawable.img_guide_background_2));
		this.mGuideImages.add(getGuideEntryView(2130837964, R.drawable.img_guide_background_3));
	}
	
	private View getGuideImageView(int resId){
		ImageView localImageView = new ImageView(this);
		localImageView.setBackgroundResource(resId);
		localImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
		localImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		return localImageView;
	}
	
	private View getGuideEntryView(int imgResId, int imgBackId) {
		View view = getLayoutInflater().inflate(R.layout.guide_entry_page_layout, null);
		this.mGuideEntry = ((TextView)view.findViewById(R.id.guide_entry));
		this.mGuideEntry.setOnClickListener(this);
		ImageView imgView = (ImageView)view.findViewById(R.id.image_guide);
		imgView.setImageResource(imgResId);
		imgView.setBackgroundResource(imgBackId);
		imgView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		return view;
	}
	
	private void initImageDot(){
		this.mGuideDotsView.removeAllViews();
		for (int i = 0; i < this.mGuideImages.size(); i++)
			addImageDot();
		this.mImageDot.getViewTreeObserver().addOnPreDrawListener(
				new ViewTreeObserver.OnPreDrawListener(){
					public boolean onPreDraw(){
					//	GuideActivity.access$002(GuideActivity.this, GuideActivity.this.mImageDot.getWidth());
						return true;
					}
				});
	}
	
	private void addImageDot(){
		ImageView img = new ImageView(this);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-2, -2);
		lp.width = DisplayConfig.getSuitableDensity(GUIDE_DOT_IMAGE_WIDTH);
		lp.weight = 1.0F;
		img.setLayoutParams(lp);
		img.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		img.setImageResource(R.drawable.img_guide_dot_normal);
		this.mGuideDotsView.addView(img);
	}

	
	private void setPageAdapter(){
		this.mAdapter = new MyPagerAdapter(this.mGuideImages);
		this.mViewPager = ((ViewPager)findViewById(R.id.contentPager));
		this.mViewPager.setAdapter(this.mAdapter);
		this.mViewPager.setOnPageChangeListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == this.mGuideEntry)
		      startMainActivity();
	}
	
	private void startMainActivity(){
		startActivity(new Intent(this, DragableActivity.class).setData(getIntent().getData()).putExtras(getIntent()));
		finish();
	}

	public void onBackPressed() {
		startMainActivity();
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
	}
	
	private void moveCursorTo(int paramInt){
		TranslateAnimation translateAnimation = new TranslateAnimation(this.mOffset * this.mDotPos, 
				paramInt * this.mOffset, 0.0F, 0.0F);
		translateAnimation.setDuration(300L);
		translateAnimation.setFillAfter(true);
		this.mImageDot.startAnimation(translateAnimation);
	}

	@Override
	public void onPageSelected(int arg0) {
		moveCursorTo(arg0);
		this.mDotPos = arg0;
		int i = this.mAdapter.getCount();
		if (!this.mIsShowGuidePage)
			if (arg0 < i - 1){
				this.mIsShowGuidePage = false;
			}else{
				this.mIsShowGuidePage = true;
			}
	}
	
	public class MyPagerAdapter extends PagerAdapter{
		private List<View> viewList;
		
		public MyPagerAdapter(List<View> viewList){
			this.viewList = viewList;
		}
		
		public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject){
			paramViewGroup.removeView((View)paramObject);
		}
		
		public int getCount(){
			return this.viewList.size();
		}
		
		public Object instantiateItem(ViewGroup paramViewGroup, int paramInt){
			View localView = (View)this.viewList.get(paramInt);
			paramViewGroup.addView(localView, 0);
			return localView;
		}
		
		public boolean isViewFromObject(View paramView, Object paramObject) {
			return paramView == paramObject;
		}
		
		public void restoreState(Parcelable paramParcelable, ClassLoader paramClassLoader) {
		}
		
		public Parcelable saveState() {
			return null;
		}
	}

}
