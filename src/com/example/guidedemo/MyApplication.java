package com.example.guidedemo;

import android.app.Application;

public class MyApplication extends Application {
	public static MyApplication mApplication = null;

	@Override
	public void onCreate() {
		super.onCreate();
		mApplication = this;
	}

}
