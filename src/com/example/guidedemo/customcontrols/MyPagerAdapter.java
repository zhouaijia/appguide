package com.example.guidedemo.customcontrols;

import java.util.ArrayList;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

public class MyPagerAdapter extends PagerAdapter {

	private ArrayList<View> mViewList;
	private boolean b = false;
	
	public MyPagerAdapter(ArrayList<View> list){
		mViewList = list;
	}
	
	public MyPagerAdapter(ArrayList<View> list, byte paramByte){
		mViewList = list;
		this.b = true;
	}
	
	@Override
	public final void destroyItem(View v, int position, Object o) {
		((ViewPager)v).removeView((View)mViewList.get(position));
	}
	
	@Override
	public final void finishUpdate(View v){
		//if (this.b)
		//	BookShelfTabActivity.a.a();
	}
	
	@Override
	public final int getCount() {
		if (mViewList != null)
			return mViewList.size();
		return 0;
	}
	
	@Override
	public final Object instantiateItem(View v, int position){
		try{
			if (((View)mViewList.get(position)).getParent() == null)
				((ViewPager)v).addView((View)mViewList.get(position));
			return mViewList.get(position);
			
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public final boolean isViewFromObject(View v, Object o) {
		return v == o;
	}
	
	@Override
	public final void restoreState(Parcelable parcelable, ClassLoader classLoader){
	}
	
	@Override
	public final Parcelable saveState(){
		return null;
	}
	
	@Override
	public final void startUpdate(View v) {
	}
}
