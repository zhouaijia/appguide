package com.example.guidedemo.customcontrols;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

public class TabButton extends ImageView{
  private Animation a = new AlphaAnimation(0.3F, 1.0F);
  private Animation b;
  private int c = -1;
  private BitmapDrawable d;
  private int e = -1;
  private BitmapDrawable f;
  private boolean g = true;
  private boolean h = false;

  public TabButton(Context paramContext){
    this(paramContext, null);
  }

  public TabButton(Context paramContext, AttributeSet paramAttributeSet){
    super(paramContext, paramAttributeSet);
    this.a.setDuration(100L);
    this.b = new AlphaAnimation(1.0F, 0.3F);
    this.b.setDuration(100L);
  }

  public void setSelected(boolean paramBoolean){
	  super.setSelected(paramBoolean);
	  this.h = paramBoolean;
	  if (paramBoolean)
		  if (this.g)
		  {
			  this.b.setAnimationListener(new MAnimationListener());
			  startAnimation(this.b);
		  }
	  
	  if(this.d==null){
		  if (this.c != -1){
			  setImageResource(this.c);
		  }
	  }else{
		  setImageDrawable(this.d);
	  }
	  
	  if(this.f==null){
		  if (this.e != -1){
			  setImageResource(this.e);
		  }
	  }else{
		  setImageDrawable(this.f);
	  }
  }
  
  class MAnimationListener  implements Animation.AnimationListener{
	  @Override
	  public final void onAnimationEnd(Animation animation) {
	    if (TabButton.a(this.a))
	      if (c != -1)
	       setImageResource(c);
	    
	    startAnimation(a);
	    
	        if (d != null)
	          setImageDrawable(d);
	      
	      if (e != -1)
	        setImageResource(e);
	      
	    
	    if(f != null)
	    	setImageDrawable(f);
	  }

	  @Override
	  public final void onAnimationRepeat(Animation paramAnimation) {
	  }

	  @Override
	  public final void onAnimationStart(Animation paramAnimation) {
	  }
	}

  public void setTabIcon(int paramInt1, int paramInt2){
    this.c = paramInt1;
    this.e = paramInt2;
    setImageResource(this.e);
  }

  public void setTabIcon(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean){
    this.c = paramInt1;
    this.e = paramInt2;
    setImageResource(this.e);
    setBackgroundResource(paramInt3);
    this.g = paramBoolean;
  }

  public void setTabIcon(BitmapDrawable paramBitmapDrawable1, BitmapDrawable paramBitmapDrawable2){
    this.d = paramBitmapDrawable1;
    this.f = paramBitmapDrawable2;
    setBackgroundDrawable(this.f);
  }
}