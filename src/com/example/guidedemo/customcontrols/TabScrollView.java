package com.example.guidedemo.customcontrols;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Scroller;

public class TabScrollView extends View{
  private int a = 0;
  private int curRow = 0;
  private int tabCount = 4;
  private int childViewWidth = 0;
  private int e = 0;
  private int f = 0;
  private int comeFromWhere = 111;
  private Drawable selectedTabBg;
  private MyScrollerTask scrollTask;

  public TabScrollView(Context context, AttributeSet attributeSet) {
    super(context, attributeSet);
  }

  private int getChildViewWidth(){
	  if (comeFromWhere == 211){
		  childViewWidth = getWidth() / 4;
	  }else{
		  childViewWidth = getWidth() / tabCount;
	  }
	  
	  return childViewWidth;
  }

  @Override
  protected void onDraw(Canvas canvas){
    super.onDraw(canvas);
    canvas.translate(this.a, 0.0F);
    selectedTabBg.draw(canvas);
  }

  @Override
  protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);
    if (childViewWidth == 0){
      if ((comeFromWhere == 211) && (curRow == 0))
        selectedTabBg.setBounds(0, 0, getChildViewWidth() << 1, getBottom() - getTop());
    }
    else
      return;
    selectedTabBg.setBounds(0, 0, getChildViewWidth(), getBottom() - getTop());
    setTargetRow(curRow);
  }

  public void setCurrentRow(int row) {
    curRow = row;
  }

  public void setData(int paramInt1, int paramInt2, int drawableResId) {
    tabCount = paramInt1;
    comeFromWhere = paramInt2;
    selectedTabBg = getContext().getResources().getDrawable(drawableResId);
  }
  
  class MyScrollerTask  implements Runnable{
	  private Scroller scroller = new Scroller(getContext());

	  public void abortAnim() {
		  scroller.abortAnimation();
	  }

	  public void startAnim(int dx) {
		  scroller.startScroll(childViewWidth, e, dx, f, 400);
		  post(this);
	  }

	  public void run() {
	    if (scroller.computeScrollOffset()) {
	     // TabScrollView.a(curRow, scroller.getCurrX());
	    	a = scroller.getCurrX();
	      if (comeFromWhere == 211)
	    	  selectedTabBg.setBounds(0, 0, scroller.getCurrY(), getBottom() - getTop());
	      invalidate();
	      post(this);
	      return;
	    }
	    scroller.abortAnimation();
	  }
	}

  public void setTargetRow(int target) {
    if ((curRow == target) && (comeFromWhere == 211))
    	return;
    
    int scrollDx = 0;
    if (comeFromWhere == 211){
    	if (curRow == 0) {
    		this.e = childViewWidth << 1;
    		this.f = -childViewWidth;
    		
    		if (target != 0)
    			scrollDx = childViewWidth * (target + 1) - this.a;
    		else
    			scrollDx = -this.a;
    	}
    }else{
    	if (target == 0){
    		this.e = childViewWidth;
    		this.f = childViewWidth;
    	}else{
    		this.e = childViewWidth;
    		this.f = 0;
    	}
    	
    	scrollDx = target * childViewWidth - this.a;
    }
    
    
    if (scrollTask == null)
    	scrollTask = new MyScrollerTask();
    scrollTask.abortAnim();
    scrollTask.startAnim(scrollDx);
    
    curRow = target;
    this.e = 0;
    this.f = 0;
    
  }
}