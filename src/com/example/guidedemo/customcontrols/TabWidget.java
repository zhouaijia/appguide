package com.example.guidedemo.customcontrols;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class TabWidget extends LinearLayout implements View.OnFocusChangeListener{
	  private TabSelectionListener mTabSelectionListener;
	  private int curSelectedTab = 0;

	  public TabWidget(Context context){
	    this(context, null);
	  }

	  public TabWidget(Context context, AttributeSet attributeSet) {
	    super(context, attributeSet);
	    setOrientation(0);
	    setFocusable(true);
	    setFocusableInTouchMode(true);
	    setOnFocusChangeListener(this);
	  }

	  public final void addView(View v, int paramInt){
		  if (v.getLayoutParams() == null) {
			  if (paramInt != 0){
				  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, -2, 1.0F);
				  lp.setMargins(0, 0, 0, 0);
				  v.setLayoutParams(lp);
			  } else{
				  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, -2, 2.0F);
				  lp.setMargins(0, 0, 0, 0);
				  v.setLayoutParams(lp);
			  }
		  }
		  v.setFocusable(true);
		  v.setClickable(true);
		  v.setOnClickListener(new OnClickListener() {
			  @Override
			  public void onClick(View v) {
				  setCurrentTab(getChildCount() - 1);
				  if (mTabSelectionListener != null)
					  mTabSelectionListener.setCurTab(getChildCount() - 1);
			  }
		  });//new d(this, -1 + getChildCount(), (byte)0)
		  v.setOnFocusChangeListener(this);
		  super.addView(v);
	  }

	  @Override
	  public void addView(View v){
		  if (v.getLayoutParams() == null){
			  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, -2, 1.0F);
			  lp.setMargins(0, 0, 0, 0);
			  v.setLayoutParams(lp);
		  }
		  v.setFocusable(true);
		  v.setClickable(true);
		  v.setOnClickListener(new OnClickListener() {
			  @Override
			  public void onClick(View v) {
				  setCurrentTab(getChildCount() - 1);
				  if (mTabSelectionListener != null)
					  mTabSelectionListener.setCurTab(getChildCount() - 1);
			  }
		  });//new d(this, -1 + getChildCount(), (byte)0)
		  v.setOnFocusChangeListener(this);
		  super.addView(v);
	  }

	  @Override
	  public void onFocusChange(View v, boolean hasFocus){
	    if ((v == this) && (hasFocus))
	      getChildAt(curSelectedTab).requestFocus();
	    
	      if (hasFocus){
	    	  for(int i=0;i<getChildCount();i++){
	    		  if (getChildAt(i) == v) {
		            setCurrentTab(i);
		            if (mTabSelectionListener != null)
		            	mTabSelectionListener.setCurTab(i);
		          }
	    	  }
	      }
	  }

	  public void setCurrentTab(int selectedTab){
	    if ((selectedTab < 0) || (selectedTab >= getChildCount()))
	      return;
	    
	    for (int i = 0; i < getChildCount(); i++){
	    	if (i != selectedTab){
	    		getChildAt(i).clearAnimation();
	    		getChildAt(i).setSelected(false);
	    	}
	    }
	    curSelectedTab = selectedTab;
	    getChildAt(curSelectedTab).setSelected(true);
	    
	  }
	  
	  public abstract interface TabSelectionListener{
		  public abstract void setCurTab(int paramInt);
	  }
	  
	  public void setTabSelectionListener(TabSelectionListener listener){
	    mTabSelectionListener = listener;
	  }
}