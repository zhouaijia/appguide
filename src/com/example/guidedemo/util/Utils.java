package com.example.guidedemo.util;

import android.content.SharedPreferences;

import com.example.guidedemo.MyApplication;


public class Utils {

	  public static int getDensity(float paramFloat){
	    return (int)(0.5D + paramFloat * MyApplication.mApplication.getResources().getDisplayMetrics().density);
	  }

	  public static int stringToInt(String str){
	    return Integer.parseInt(str.substring(4, str.lastIndexOf(".")));
	  }
	  
	  private static SharedPreferences getSharedPreferences(){
		    return MyApplication.mApplication.getSharedPreferences("APP_FLAG", 3);
		  }
	  
	  public static void  setBookShelfLayoutTop(String key, boolean value){
		  SharedPreferences.Editor editor = getSharedPreferences().edit();
		  editor.putBoolean("bookshelfLayoutTop", true);
		  editor.commit();
	  }
	
	  public static boolean getBookShelfLayoutTop(String key, boolean defaultvalue){
		  return getSharedPreferences().getBoolean(key, defaultvalue);
	  }
}
